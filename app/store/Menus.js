Ext.define('Agricultor.store.Menus', {
	extend : 'Ext.data.TreeStore',
	model : 'Agricultor.model.Menu',
	proxy : {
		type : 'ajax',
		url : 'php/menu.php',
		reader : {
			type : 'json'
		}
	}
/*
 * , autoLoad: false, root: { expanded: false // loaded:true }
 */
/*
 * como por defeito expanded: true, a árvore é logo lida, mesmo com autoLoad:
 * false.
 * http://www.sencha.com/forum/showthread.php?136426-TreeStore-ignores-autoLoad-false
 */
});