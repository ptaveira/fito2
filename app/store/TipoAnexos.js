Ext.define('Agricultor.store.TipoAnexos', {
	extend : 'Ext.data.Store',
	model : 'Agricultor.model.TipoAnexo',
	autoLoad: false, // após o render da grid
	autoSync : true,
	pageSize : 35,
	proxy : {
		type : 'ajax',
		extraParams : {
			tabela : 'tipoanexo',
			root : 'tipoanexos'
		},
		api : {
			create : 'php/cria.php',
			read : 'php/lista.php',
			update : 'php/atualiza.php',
			destroy : 'php/remove.php'
		},
		reader : {
			type : 'json',
			root : 'tipoanexos',
			successProperty : 'success'
		},
		writer : {
			type : 'json',
			writeAllFields : false,
			encode : true,
			root : 'tipoanexos'
		}
	}
});