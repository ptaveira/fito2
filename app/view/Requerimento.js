Ext.define('Agricultor.view.Requerimento', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.requerimento',
	requires : ['Ext.form.Label'],
	title : 'Requerimentos',
	store : 'Requerimentos',
	columnLines : true,
	columns : [{
		header : 'Id',
		dataIndex : 'id',
		width : 20
	}, {
		header : 'Nome',
		dataIndex : 'nome',
		flex : 1
	}, {
		header : 'Data de entrada',
		dataIndex : 'datacriacao',
		xtype : 'datecolumn',
		width : 120,
		format : 'Y-m-j H:i:s',
		filter : true
	}, {
		header : 'Estado',
		dataIndex : 'designacao',
		width : 120
	}],
	dockedItems : [{
		xtype : 'toolbar',
		flex : 1,
		dock : 'top',
		items : [{
			xtype : 'button',
			text : 'Criar',
			itemId : 'cria'
		}, {
			xtype : 'button',
			text : 'Modificar',
			itemId : 'modifica'
		}, {
			xtype : 'button',
			text : 'Apagar',
			itemId : 'apaga'
		}]
	}]
});
