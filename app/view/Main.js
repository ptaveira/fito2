Ext.define('Agricultor.view.Main', {
	extend : 'Ext.container.Container',
	requires : ['Ext.tab.Panel', 'Ext.layout.container.Border'],
	xtype : 'app-main',
	layout : {
		type : 'border'
	},
	items : [{
		region : 'north',
		xtype : 'cabecalho'
	}, {
		title : 'Menu',
		width : 185,
		collapsible : true,
		region : 'west',
		// xtype : 'panel',
		xtype : 'menuprincipal'
	}, {
		region : 'center',
		xtype : 'tabpanel',
		items : [{
			title : 'Apresentação',
			html : 'Bem vindo.<br/>Esta aplicação faz a gestão de pedidos de aplicadores e comerciantes de produtos fitofarmacêuticos.'
		}]
	}, {
		region : 'south',
		xtype : 'panel',
		height : 40,
		html : 'Aplicação desenvolvida pela <b>DRAPN</b>, na Quinta do Valongo'
	}]
});
