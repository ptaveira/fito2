Ext.define('Agricultor.model.Requerimento', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id', type: 'int' },
        { name: 'nome', type: 'auto' },
        { name: 'datacriacao', type: 'date' },
        { name: 'designacao', type: 'auto' }
    ]
});
