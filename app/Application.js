Ext.define('Agricultor.Application', {
	name : 'Agricultor',
	extend : 'Ext.app.Application',
	requires: [ 'Ext.state.LocalStorageProvider' ],
	views : ["Cabecalho", "Grupo", "Menu", "TipoAnexo", "Requerimento"],
	controllers : ["Cabecalho", "Grupo", "Menu", "TipoAnexo", "Requerimento"],
	stores : ["Grupos", "Menus", "TipoAnexos", "Requerimentos"],
	launch : function() {
		Ext.tip.QuickTipManager.init();
		console.log('... tudo carregado e pronto a funcionar (app/Application.js).');
		if (Ext.supports.LocalStorage) {
			Ext.state.Manager.setProvider(Ext.create('Ext.state.LocalStorageProvider'));
			console.log('Vai usar local storage HTML 5');
		} else {
			Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));
			console.log('Vai usar cookies');
		}
						
		Agricultor.user = Ext.state.Manager.get("utilizador");
		// rever esta parte...
		// talvez seja melhor ir ver ao PHP se ainda tenho a sessão ativa
		if (Agricultor.user == null) {
			console.log('Antes NÃO estava loginado Agricultor.user == null');
			// this.fireEvent('logoutComSucesso');
		} else {
			console.log('Antes estava loginado');
			Ext.Ajax.request({
				scope : this,
				url : 'php/alive.php',
				success : function(conn, response, options, eOpts) {
					console.debug(conn);
					var resultado = Ext.JSON.decode(conn.responseText);
					if (resultado.success) {
						// continuamos loginados
						console.log('continuamos loginados');
						this.fireEvent('loginComSucesso');
					} else {
						// perdemos a sessão do PHP
						console.log('perdemos a sessão do PHP');
						this.fireEvent('logoutComSucesso');
					}
				},
				failure : function(conn, response, options, eOpts) {
					Ext.Msg.show({
						title : 'Erro na verificação da sessão',
						msg : conn.responseText,
						icon : Ext.Msg.ERROR,
						buttons : Ext.Msg.OK
					});
				}
			});
		}
	}
});
