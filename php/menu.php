<?php
// chama o arquivo de conexão com o bd
include ("db.php");
// curl http://localhost/git/fito/php/menu.php
 
session_start();

if (isset($_SESSION['identificado']) && $_SESSION['identificado']) {
	$grupo = $_SESSION['grupo'];
	$restricao = "where id in (select idmenu from permissao where idgrupo = " . $grupo . ") ";
} else {
	$restricao = "where anonimo ";
}
 
$sql  = "select * from menu ";
$sql .= $restricao;
$sql .= "order by idsuperior desc";
 
$res = &$mdb2->query ( $sql );
if (PEAR::isError ( $res )) {
	$result ["success"] = false;
	$result ["errors"] ["reason"] = $res->getMessage ();
	$result ["errors"] ["query"] = $sql;
} else {
	$tabela = array ();
	while ( $row = $res->fetchRow ( MDB2_FETCHMODE_ASSOC ) ) {
		if (isset($row['idsuperior'])) {
			$row['leaf'] = true;
			$idsuperior = $row['idsuperior'];
			unset ( $row['idsuperior'] );
			// unset ( $row['id'] );
			array_push ( $tabela[$idsuperior]['children'], $row);
		} else {
			unset ( $row['idsuperior'] );
			$id = $row['id'];
			// unset ( $row['id'] );
			$row['expanded'] = true;
			$tabela[$id] = $row;
			$tabela[$id]['children'] = array();
		}
	}
	$novatabela = array ();
	foreach( $tabela as $key => $value ){
		array_push($novatabela, $value);
	}
	$result['success'] = true;
	$result['sql'] = $sql;
	$result['children'] = $novatabela;
}
header ( 'Content-type: application/json' );
echo json_encode ( $result );
// echo json_encode ( $novatabela );
$mdb2->disconnect ();
?>